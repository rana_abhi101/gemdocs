# script to create a class for mobile


class Mobile:

    def __init__(self, name, imei_code, sims_available, processor_available, sim_card_available, bluetooth_status, wifi_status):
        self.imei_code = imei_code
        self.sims_available = sims_available
        self.processor_available = processor_available
        self.sim_card_available = sim_card_available
        self.bluetooth_status = bluetooth_status
        self.wifi_status = wifi_status

    def switchBluetooth(self):
        if self.bluetooth_status == True:
            self.bluetooth_status = False
            print('Bluetooth turned off')
        elif self.bluetooth_status == False:
            self.bluetooth_status = True
            print('Bluetooth turned on')
        else:
            print('Bluetooth status unknown')


    # def dial_number():


    def get_imei_code(self):
        print('Imei code : ' + self.imei_code)

    def swtich_wifi_conn(self):
        if self.wifi_status == True:
            self.wifi_status = False
            print('Wifi turned off')
        elif self.wifi_status == False:
            self.wifi_status = True
            print('Wifi turned on')
        else:
            print('Wifi status unknown')


    # def recieve_message():


    # def send_message():
