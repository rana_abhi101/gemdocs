class BankAccount:
    def __init__(self):
        self.balance = 0

    def checkBalance(self, amount):
        if amount > self.balance:
            return False
        else:
            return True

    def withDraw(self, amount):
        if self.checkBalance(amount):
            self.balance -= amount
            print('Balance now is : ' + str(self.balance))
        else:
            print('Transaction failed : Not Enough Balance')
        # self.balance -= amount


    def deposit(self, amount):
        self.balance += amount
        print('Amount deposit done and balance now is : ' + str(self.balance))
