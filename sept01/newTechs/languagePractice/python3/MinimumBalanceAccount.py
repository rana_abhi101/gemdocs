from Bank import BankAccount

class MinimumBalanceAccount(BankAccount):

    def __init__(self, minimumBalance):
        BankAccount.__init__ = self
        self.minimumBalance = minimumBalance

    def withDraw(self,amount):
        if self.balance - amount < self.minimumBalance:
            print('Sorry, minimum balance must be maintained.')
        else:
            BankAccount.withdraw(self, amount)

    def deposit(self,amount):
        BankAccount.deposit(self, amount)
