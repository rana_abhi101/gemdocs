import time
import threading

class CountdownThread(threading.Thread):
    def __init__(self,count):
        threading.Thread.__init__(self)
        self.count = count

    def run(self):
        while self.count > 0:
            print('COunting down : ' + str(self.count))
            self.count -= 1
            time.sleep(4)

        return


def main():
    cl = CountdownThread(4)
    cl.run()

if __name__ == '__main__':
    main()
