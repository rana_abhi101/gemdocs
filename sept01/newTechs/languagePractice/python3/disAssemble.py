# script to disassemble a python script with dis module
import dis

def lopp():
    for i in range(20):
        print(i)

def main():
    dis.dis(lopp)

if __name__ == '__main__':
    main()

