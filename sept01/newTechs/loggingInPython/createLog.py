import logging
import time

def getTime():
    now = time.strftime("%c")
    return now
def createEntryToLog():
    logging.basicConfig(filename='test.log', level=logging.DEBUG)
    logging.info('[' + str(getTime()) + ']   createLog.py file is run')
    logging.warning('[' + str(getTime()) + ']  runnin the py file')

def main():
    createEntryToLog()

if __name__ == '__main__':
    main()
