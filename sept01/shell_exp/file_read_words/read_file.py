filePath = 'file.txt'

def file_size(filePath):
    with open(filePath, 'r') as file:
        filename = file.read()
        len_chars = sum(len(word) for word in filename)
        return len_chars

def main():
    chars = file_size(filePath)
    print(chars)

if __name__ == '__main__':
    main()
