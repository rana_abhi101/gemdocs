
# inner_list = []
# outer_list = []
# m = 2
# n = 2
# for line in range(m):
#     inner_list = map(str, input().split(None, n)[:n])
#
#     outer_list.append(inner_list)
#
# print(outer_list[2][3])
import itertools
def main():
    import itertools
    inner_list = []
    outer_list = []
    main_list_of_words = []
    universal_combination_list = []
    num_words = input()
    dict_words = input().split()
    m,n = input().split()
    for line in range(m):
        inner_list = map(str, input().split(None, n)[:n])
        mainListAppendOfWord(inner_list)
        outer_list.append(inner_list)
    # sorted list of words
    sortedWordsList = sorted(dict_words, key=len)
    minVal = len(sortedWordsList[0])
    maxVal = len(sortedWordsList[-1])

def mainListAppendOfWord(list_of_words):
    for val in list_of_words:
        main_list_of_words.append(val)

def combinations_list_word(minVal, maxVal, list_words):
    for i in range(minVal,maxVal+1):
        word = ''
        for xs in itertools.product(list_words, repeat=i):
            word = ''.join(xs)
        universal_combination_list.append(word)

# def combinations_list_word():
#     chrs = 'abc'
#     for i in range(3,4):
#       for xs in itertools.product(chrs, repeat=i):
#         print(xs)
#         # print (''.join(xs))
#         universal_combination_list.append()

if __name__ == '__main__':
    combinations_list_word()
