# reading a csv for athletics data
import pandas as pd
import numpy as np
from collections import Counter as ct
import matplotlib.pyplot as plt


file_path = '2014_data/olympicAthletes.csv'

def readData(file):
	dataFrame = pd.read_csv(file, sep=None,engine='python')

	saved_column = dataFrame['Sport']
	# print(saved_column)

	identifyCategories(saved_column)

def identifyCategories(column_list):
	output_columns = list()
	output_counts = list()

	count_dict = {}
	for val in column_list:
		if val not in output_columns:
			output_columns.append(val)

	# print(output)
	counts = ct(column_list)
	print('Total count is : ' + str(len(output_columns)))
	print(str(len(column_list)))
	# print(counts)
	sum = 0
	for key in counts:
		val = counts[key]
		count_dict[key] = val # adding it to the dictionary
		output_counts.append(val)# adding the val to another list
		sum += int(val)
	print(count_dict)
	print(sum)
	
	output_counts_set = set(output_counts)
	print(output_counts_set)
	# plotPieChart(output_columns, output_counts)
	barChart(output_columns, output_counts)

def plotPieChart(nameList, countList):
	labels = nameList
	sizes = countList

	plt.pie(sizes, labels=labels , shadow=True, startangle=140)
	plt.axis('equal')
	plt.show()

def barChart(nameList, countList):
	objects = nameList
	y_pos = np.arange(len(objects))

	performance = countList

	plt.barh(y_pos, performance, align='center', alpha=0.5)
	plt.yticks(y_pos,objects)
	plt.xlabel('Counts')
	plt.title('types of athletes')

	plt.show()

def main():
	readData(file_path)

if __name__ == '__main__':
	main()
