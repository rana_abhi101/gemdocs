- pragmas = are the use module like statements
- comments # single line comment 
has no block line comments
- variables = scalars($), arrays(@) and hashes(%)
- variable declaration = 'my' keyword
- scalar = undef, number, string, reference
- booleans = has no boolean data type
- statement in if is false for 0, empty and undef and "0"
- Function = return true and returns "" if false
- numerical operators: <,>,<=,=>,!=,<=>,+,*
- string operators: lt,gt,le,ge,eq,ne,cmp,.,x

