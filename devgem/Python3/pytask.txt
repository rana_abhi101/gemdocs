Write a script in Python & Perl (both) to read the attached excel file (Globa_PMI_Data_..),
 parse the data and plot the graph as shown in ‘PMI Chart’ sheet,
 need to export the graph to a html & pdf both
file name & location should be taken from command line
Once the report has been generated,
we need to archive the file from input location to archival directory,
compress the file while archival
Need to remove archived files if older than 90 days
