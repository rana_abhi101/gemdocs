#!/usr/bin/python3
import sys

for arg in sys.argv:
    print(arg)

def printargs(**args):
    print('Printing the args')
    for arg in args:
        print('this argument: ' + arg)

printargs(sys.argv)
