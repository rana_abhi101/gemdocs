#!/usr/bin/python3
#example for using re module in python3

import re

str = 'an example word:cat!!'
match = re.search(r'word:\w\w\w',  str)
#if statement after the search 
if match:
	print("found: " + match.group())
else:
	print('Nothing found')

