# #!/usr/bin/python3
# #script to find the day when a date is entered
#
# import datetime as dt
# # import time as tt
# days = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
# day, month, year = input("Enter in format day month year(all integers): ").split()
#
# dateEntered = dt.date(int(year), int(month), int(day))
# # dateEntered.strftime("%d-%B-%Y"))
# weekDay = dateEntered.weekday()
# print(days[weekDay])

import datetime as dt
days=["MONDAY","TUESDAY","WEDNESDAY","THURSDAY","FRIDAY","SATURDAY","SUNDAY"]
month, day, year = input().split()
if int(year) > 2000 and int(year) < 3000:
    dateEntered = dt.date(int(year), int(month), int(day))

    weekDay = dateEntered.weekday()
    print(days[weekDay])
