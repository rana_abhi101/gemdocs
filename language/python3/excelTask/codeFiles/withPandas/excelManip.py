#!/usr/bin/python3

import pandas as pd

path = '../../dataFiles/data.xlsx'
def readExcel(file):
    work_book = pd.read_excel(file, sheetname=0)
    print(work_book.head())

def main():
    readExcel(path)

if __name__ == '__main__':
    main()
