#!/usr/bin/python3

# a = input().split()
valsEntered = []
columnsNames = ['s.no','name','year','join','age','company']

def enterValues():
    for index,colNam in enumerate(columnsNames):
        val = input('Enter '+colNam+': ')
        valsEntered.append(val)

    displayValues()

def displayValues():
    print(valsEntered)

def main():
    enterValues()
if __name__ == '__main__':
    main()
