#!/usr/bin/python3

import xlrd as xl
path = '../dataFiles/data.xlsx'
def readFile(file):
    excel_book = xl.open_workbook(file)
    # displayExcelDetails(excel_book)
    readSheet(excel_book)

def displayExcelDetails(book):
    sheetnames = book.sheet_names()
    for index,sheet in enumerate(sheetnames):
        print(sheet+' is sheet no. '+str(index))
    #for number of sheets
    print('this excel has '+ book.nsheets)

def readSheet(book):
    first_sheet = book.sheet_by_index(0)
    print('displaying the first row')
    print(first_sheet.row_values(0))

    takeDataToFeed(book)

def takeDataToFeed(book):
    valuesEntered = []
    cols = book.sheet_by_index(0).row_values(0)
    for index,colNam in enumerate(cols):
        val = input('Enter '+ colNam + ': ')
        valuesEntered.append(val)
    print('The values entered are: '+ str(valuesEntered))
    writeToExcel(valuesEntered)

def writeToExcel(valuesToWrite, book):
    sheetToEnterData = book.sheet_by_index(0)
    sheetToEnterData.write(valuesToWrite)

    book.save()


def main():
    readFile(path)

if __name__ == '__main__':
    main()
