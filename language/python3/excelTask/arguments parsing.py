# arguments parsing
import argparse
import sys

# def parse_cmdline_args():
# 	parser = argparse.ArgumentParser(
# 		description='This is a program to reformat json'
# 		)
# 	parser.add_arguments('--service','-s',dest='service',
# 		required=True,help="Please specify the service name file: Mandatory field"
# 		)
# 	args = parser.parse_args()

# 	if args.enable and args.disable:
# 		print("Both enable/disable cannot be passed at once")
# 		sys.exit(1)

# 	return args

# def main():
# 	arguments = parse_cmdline_args()
# 	print(arguments)

# if __name__ == "__main__":
# 	main()

def parse_arguments():
	parser = argparse.ArgumentParser(description="this is a script to set handover")

	parser.add_argument('-s','--service',help='service name')
	parser.add_argument('-a','--adder', help='servers')

	args = parser.parse_args()

	return args

def main():
	arguments = parse_arguments()
	print(arguments)

if __name__ == "__main__":
	main()