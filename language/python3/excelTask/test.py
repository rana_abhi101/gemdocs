#!/usr/bin/python3

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

me = "abhisek.rana@geminisolutions.in"
my_password = r"AbhiRana.101"
you = "abhisekrana202@gmail.com"

msg = MIMEMultipart('alternative')
msg['Subject'] = "Alert"
msg['From'] = me
msg['To'] = you
def setMessageMail():
	html = '<html><body><p>Hi, I have the following alerts for you!</p>'
	# table 1
	html += '<h2>table 2 report</h2>'
	html += '<div align="center"><h1>Handovers</h1><table border="1" width="1024" cellpadding="0" cellspacing="0"style="font-size:20px"><tr align="left" bgcolor="#FF0000"><th>Job id</th><th>Last name</th><th>Age</th></tr><tr align="left"><td>Jill</td><td>Jill</td><td>Jill</td></tr><tr><td>eves</td><td>mathews</td><td>Age</td></tr></table></div>'
	# table 2
	html += '<h2>table 2 report</h2>'
	html += '<div align="center"><h1>Handovers</h1><table border="1" width="1024" cellpadding="0" cellspacing="0" style="font-size:20px"><tr align="left" bgcolor="##32CBFF"><th>Job id</th><th>Last name</th><th>Age</th></tr><tr align="left"><td>Jill</td><td>Jill</td><td>Jill</td></tr><tr><td>eves</td><td>mathews</td><td>Age</td></tr></table></div>'
	html += '</body></html>'
	part2 = MIMEText(html, 'html')

	msg.attach(part2)

	sendMail(msg)

def setHtmlData():
	html = """<!DOCTYPE html>
<!DOCTYPE html>
<html>
<body>
	<div align="center">
		<h1>Handovers</h1>
		<table border="1" width="1024" cellpadding="0" cellspacing="0">
			<col width="80">
			<col width="600">
			<col width="100">
			<col width="70">
			<col width="70">
			<tr align="left" bgcolor="#4F81BD"  style="font-size: 10px;">
				<th>Ticket Id</th>
				<th>SUMMARY</th>
				<th>Team Responsible</th>
				<th>Duration</th>
				<th>Cycle</th>
			</tr>
			<tr align="left">
				<td>Jill</td>
				<td>Jill</td>
				<td>Jill</td>
				<td>Jill</td>
				<td>Jill</td>
			</tr>
			<tr >
				<td>eves</td>
				<td>mathews</td>
				<td>Age</td>
				<td>Jill</td>
				<td>Jill</td>
			</tr>
		</table>
		<h1>Handovers</h1>
		<table border="1" width="1024" cellpadding="0" cellspacing="0" style="font-size: 20px">
			<tr align="left" bgcolor="#4F81BD">
				<th>Ticket Id</th>
				<th>SUMMARY</th>
				<th>Team Responsible</th>
				<th>Duration</th>
				<th>Cycle</th>
			</tr>
			<tr align="left">
				<td>Jill</td>
				<td>Jill</td>
				<td>Jill</td>
				<td>Jill</td>
				<td>Jill</td>
			</tr>
			<tr >
				<td>eves</td>
				<td>mathews</td>
				<td>Age</td>
				<td>Jill</td>
				<td>Jill</td>
			</tr>
		</table>
	</div>
</body>
</html>
	"""
	part2 = MIMEText(html, 'html')

	msg.attach(part2)
	sendMail(msg)

def sendMail(msg):
	# Send the message via gmail's regular server, over SSL - passwords are being sent, afterall
	server = smtplib.SMTP_SSL('smtp.gmail.com')
	# uncomment if interested in the actual smtp conversation
	# s.set_debuglevel(1)
	# do the smtp auth; sends ehlo if it hasn't been sent already
	server.login(me, my_password)

	server.sendmail(me, you, msg.as_string())
	server.quit()

def main():
	# setMessageMail()
	setHtmlData()

if __name__ == '__main__':
	main()