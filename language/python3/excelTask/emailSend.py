# script to send email with smtlib

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
SERVER = "localhost"

me = "abhisek.rana@geminisolutions.in"
to = "abhisekrana202@gmail.com"
cc = ""
bcc = ""

rcpt = cc.split(",") + bcc.split(",") + [to]
msg = MIMEMultipart('alternative')
msg['Subject'] = "my subject"
msg['To'] = to
msg['Cc'] = cc
msg['Bcc'] = bcc

my_msg_body = "the email is sent via smtplib"

msg.attach(MIMEText(my_msg_body.strip()))
# server = smtplib.SMTP("localhost") # or your smtp server
# server.sendmail(me,rcpt,msg.as_string())
# server.quit()
server = smtplib.SMTP(SERVER, 1025)
server.sendmail(me, rcpt, msg.as_string())
server.quit()