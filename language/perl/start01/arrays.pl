#!/usr/bin/perl

use strict;
use warnings;

my @arr1 = (
	"print",
	"these",
	"strings",
	"out",
	"for",
	"me",
);

print $arr1[0];
print "\n";
#printing the values with negative index
print $arr1[-1];
print "\n";
#printing the completer array without space
print @arr1;
print "\n";

#finding the number of elements in an array
print "The array has : ".(scalar @arr1)." elements";
print "\n";

#finding the last populated index
print "the last populated index was : ".$#arr1;
print "\n";

#printing out all the array elements
print "@arr1";
print "\n";
