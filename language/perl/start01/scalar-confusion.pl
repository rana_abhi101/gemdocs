#!/usr/bin/perl

use strict;
use warnings;

my $st1 = "4G";
my $st2 = "4H";

print $st1 . $st2."\n";
print $st1 + $st2."\n";
print $st1 eq $st2."\n";
print ($st1 == $st2);
print "\n";
#the classic error
print "yes" == "no";
print "\n";
