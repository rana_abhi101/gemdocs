var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

//models for various routes
Genre = require('./models/genres')
Book = require('./models/book')

// Connect to mongoose
mongoose.connect('monogdb://localhost/bookstore');
var db = mongoose.connection;

app.get('/', function(req, res){
  // res.send('Hello bookstore');
  Genre.getGenres(function(err,genres){
    if(err){
      throw err;
    }
    res.json(genres);
  });
});

app.get('/api/genres', function(req, res){
  Genre.getGenres(function(err,genres){
    if(err){
      throw err;
    }
    res.json(genres);
  });
});

app.get('/api/books', function(req, res){
  Book.getBooks(function(err,books){
    if(err){
      throw err;
    }
    res.json(books);
  });
});


app.listen(3000);
console.log('Running bookstore on port 3000');
