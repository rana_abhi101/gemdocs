// for the genre route
var mongoose = require('mongoose');

// Genre schema
var bookSchema = mongoose.Schema({
   title:{
     type:String,
     required:true
   },
   genre:{
     type:String,
     required:true
   },
   description:{
     type:String
   },
   author:{
     type:String,
     required:true
   },
   publisher:{
     type:String
   },
   pages:{
     type:String
   },
   images_url:{
     type:String
   },
   buy_url:{
     type:String
   }
});

var Book = module.exports = mongoose.model('Book', bookSchema);

// get Books
module.exports.getBooks = function(callback, limit){
  Book.find(callback).limit(limit);
}
