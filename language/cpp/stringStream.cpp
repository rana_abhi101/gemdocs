// file to parse comma separated values in cpp
#include<iostream>
using namespace std;
#include<sstream>
#include<vector>

int main(){
  string str;
  cin >> str;
  vector<int> integers = parseInts(str);
  for(int i = 0; i < integers.size(); i++) {
      cout << integers[i] << "\n";
  }


  return 0;
}
