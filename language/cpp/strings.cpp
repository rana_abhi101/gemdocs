#include<iostream>
using namespace std;
#include<string>

string remove_first(string st){
  string new_st;
  for(int i=0;i<st.size();i++){
    if (i>0){
      new_st += st[i];
    }
  }
  return new_st;
}

int main(){
  string st1,st2,st1Temp, st2Temp;
  string new_st1, new_st2;

  cin>>st1;
  cin>>st2;
  cout<<st1.size()<<" "<<st2.size()<<"\n";
  cout<<st1+st2<<"\n";

  st1Temp = remove_first(st1);
  st2Temp = remove_first(st2);

  new_st1 = st2[0] + st1Temp;
  new_st2 = st1[0] + st2Temp;

  // cout<<st2Temp;
  cout<<new_st1<<"\n";
  cout<<new_st2;

  return 0;
}
