# bubble sort in python
import sys

terms = input()
termsArray = list(map(int, input().strip().split(' ')))

numberOfSwaps = 0
for i in range(len(termsArray)):
    for j in range(0,i):
        if termsArray[j] > termsArray[j+1]:
            #swap the values
            termsArray[j],termsArray[j+1] = termsArray[j+1],termsArray[j]
            numberOfSwaps += 1
    if numberOfSwaps == 0:
        break




print('Array is sorted in '+str(numberOfSwaps)+' swaps')
print('First Element: '+str(termsArray[0]))
print('Last Element: '+str(termsArray[-1]))
