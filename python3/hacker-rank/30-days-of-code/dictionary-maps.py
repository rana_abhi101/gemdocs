# script to take given number of inputs for dictionary
# then take unknown number of lines of keys and search it
phoneBook = {}
searchQueryName = []

iterations = input()
for each in range(0,int(iterations)):
    key,value = input().split()
    phoneBook[key] = value
while True:
    # name = input()
    try:
        name = input()
        if name == '':
            break
        else:
            searchQueryName.append(name)
    except(EOFError):
        break
for name in searchQueryName:
    if name in phoneBook.keys():
        print(name+'='+phoneBook[name])
    else:
        print('Not found')
