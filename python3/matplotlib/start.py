import bs4 as bs
import urllib.request

sauce = urllib.request.urlopen('https://pythonprogramming.net/parsememcparseface/').read()

soup = bs.BeautifulSoup(sauce, 'lxml')

print(soup)
print(soup.title)
print(soup.title.name)
print(soup.title.string)
print(soup.title.text)

print(soup.p)
print(soup.find_all('p'))

for para in soup.find_all('p'):
    print(para.string)#returns nivagable string

print(soup.get_text())

for url in soup.find_all('a'):
    print(url)#entire tag
    print(url.text)#prints name of tag
    print(url.get('href'))
