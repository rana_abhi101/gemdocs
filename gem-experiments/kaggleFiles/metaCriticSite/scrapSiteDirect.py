# basic script to scrap metascritic site directly
# link = http://www.metacritic.com
from bs4 import BeautifulSoup as bs
# import urllib.request as urlop
from urllib.request import Request, urlopen

basePath = 'http://www.metacritic.com'
tv = "/tv"

def getData(link):
    req = Request(link, headers={'User-Agent': 'Mozilla/5.0'})
    webpage = urlopen(req).read()
    soup = bs(webpage, 'lxml')
    print('And the title for this glorious site is '+soup.title.string)

# def getLink(file):

getData(basePath)
