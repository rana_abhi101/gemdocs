# script to analyse credit card fraud csv file
import pandas as pd
import numpy as np

def read_file(file):
	dataFrame = pd.read_csv(file)

	print(dataFrame.head())

def main():
	file = '../resources/creditcard_fraud/creditcard.csv'
	read_file(file)

if __name__ == '__main__':
	main()