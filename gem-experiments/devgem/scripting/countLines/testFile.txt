In computing, a line number is a method used
to specify a particular sequence of cha
racters in a text file. T
he most common method of assigning n
umbers to lines is to assign every line a uni
que number, starting at 1 for the first line,
and incrementing by 1 for each successive line.

In the C programming language the line numbe
r of a source code line is one greater than
the number of new-line characters read o
r introduced up to that point.[1]

Line numbers were a required element of syntax in some o
lder programming languages such as GW-BASIC.[
2] The primary reason for this is that most operating systems a
t the time lacked interactive text editors; s
ince the programmer's interface was usually limited to a line editor,
line numbers provided a mechanism by which specific lines in the source
code could be referenced for editing,
and by which the programmer could insert a new line at a specific poi
nt. Line numbers also provided a convenient means of distinguis
hing between code to be entered into the program and direct mode co
mmands to be executed immediately when entered by the user
which do not have line numbers.

Largely due to the prevalence of interactive text editing in modern
operating systems, line numbers are not a fea
ture of most programming languages.
