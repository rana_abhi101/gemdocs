# appending in a file based on user's choice whether to add in the
# same line or into a new line

def appendOnChoice(file, text, choice):
    f = open(file, 'a')
    if choice == 'space':
        f.write(' ' + text)
    else:
        f.writelines(text)

    f.close()
