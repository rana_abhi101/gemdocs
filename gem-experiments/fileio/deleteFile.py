import os

def deleteFile(file):
    if os.path.isfile(file):
        os.remove(file)
        print(file + ' deleted successfully')
    else :
        print('Error: %s file not found' %file)
