#!/usr/bin/python3
#without using any module reading a csv file



def openFile(path):
    file = open(path, encoding='utf-8')
    displayFileData(file)

def displayFileData(file):
    # for idx,val in enumerate(file):
    #     if idx < 10:
    #         print(val)
    print(file.read(130))

def main():
    path = '../data/test.csv'
    openFile(path)

if __name__ == '__main__':
    main()