# scraping the tv option of metascritic
from bs4 import BeautifulSoup as bs
from urllib.request import Request, urlopen

basePath = 'http://www.metacritic.com/tv'

def writeDataIntoFile(dataToWrite):
    file = open('test.html', 'w+')
    file.write(dataToWrite)
    file.close()

def getData(link):
    req = Request(link, headers={'User-Agent': 'Mozilla/5.0'})
    webpage = urlopen(req).read()
    soup = bs(webpage, 'lxml')
    return soup

def data():
    data = getData(basePath)
    htmlContent = data.prettify()
    print(data.prettify())
    writeDataIntoFile(htmlContent)

data()
