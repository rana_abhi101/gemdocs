# script to draw a scatter plot

import matplotlib.pyplot as plt

x = [1,2,3,4,5,6,7,8]
y = [5,2,4,2,1,4,5,2]

plt.scatter(x,y, label='scatter', color='r', marker='*', s=200)

plt.xlabel('x')
plt.ylabel('y')
plt.title('Interesting scatter plot')
plt.legend()
plt.show()
