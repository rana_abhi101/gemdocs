# script to add legends and title to graph
import matplotlib.pyplot as plt

x = [1,2,3]
y = [5,7,4]

x1 = [1,2,3]
y1 = [10,14,12]

plt.plot(x,y, label='plot1 is the best')
plt.plot(x1,y1, label='plot2 is the worst')
plt.xlabel('x-axis')
plt.ylabel('y-axis')

plt.title('Interesting graph')
plt.legend()
plt.show()
