# script to make bar chart and histogram
import matplotlib.pyplot as plt

x = [2,4,6,8,10]
y = [6,7,8,2,4]

x1 = [1,3,5,7,9]
y1 = [7,8,2,4,2]

plt.bar(x,y,label='Bars1',color='r')
plt.bar(x1,y1,label='Bars2',color='c')

plt.xlabel('x-axis')
plt.ylabel('y-axis')
plt.title('bar chart and histogram')
plt.legend()
plt.show()
