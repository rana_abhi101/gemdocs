#!/usr/bin/perl

use strict;
use warnings;

my %team = (
	"Newton" => "Isaac",
	"Einstein" => "Albert",
	"Darwing" => "Charles",
);

print $team{"Newton"};
print "\n";
print $team{"Dyson"};
print "\n";

# converting hash to an array
my @newTeam = %team;
print "@newTeam";
print "\n";

# converting an array to hash
my %newHash = @newTeam;
print %newHash{"Einstein"};

while ( ($k,$v) = each %newHash ) {
    print "$k => $v\n";
}
