import bs4 as bs
import urllib.request as urlop

sauce = urlop.urlopen("https://pythonprogramming.net/parsememcparseface/").read()

soup = bs.BeautifulSoup(sauce, 'lxml')

def getFirstpTag() :
    print(soup.p)

def getAllpTags(x) :
    
    print('first way = simply para')
    for para in soup.find_all('p'):
        print(para)
    print('second way = with .string')
    for para in soup.find_all('p'):
        print(para.string)
    print('third way = with .text')
    for para in soup.find_all('p'):
        print(para.text)
    print('fourth way = .get_text()')
    for para in soup.find_all('p'):
        print(para.get_text())

def getTheurlTags() :
    print('first way = simply url')
    for ur in soup.find_all('a'):
        print(ur)
    print('second way = with .text')
    for ur in soup.find_all('a'):
        print(ur.text)
    print('third way = with .string')
    for ur in soup.find_all('a'):
        print(ur.string)
    print("fourth way = with .get('href')")
    for ur in soup.find_all('a'):
        print(ur.get('href'))

# getFirstpTag()
getAllpTags()
