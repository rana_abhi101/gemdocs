from bs4 import BeautifulSoup

with open("first.html") as fp:
    soup = BeautifulSoup(fp, 'lxml')

print(soup.prettify())
