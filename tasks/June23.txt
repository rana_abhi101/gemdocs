1. inodes
link01 = http://www.grymoire.com/Unix/Inodes.html

2. find command
- behaves as recursive search if we are finding some pattern of files.
find [directory] -type [d or f] -name [nameToSearch] -perm [0(permissionNumber)] -user [username]
link01 = http://www.binarytides.com/linux-find-command-examples/

3. permissions
- r = read, w = write, x = executable
- + = add permission, - = remove permission
- users for file = user(u), group(g), other(o)
- first 3 bits are for the user, next 3 are for the group and last 3 are for all other users
